shopt -s expand_aliases
source ~/.bash_profile
source $NXT_BASH_PATH/.nxt_bash
##########################

print "USAGE => go [DIR] [REPO]";
if [[ $1 == "repos" ]]
then 
  result=$NXT_CODE_PATH
else
  [ ! -z "$1" ] && DIR=$1 || DIR="master" && dirDefaultMsg="(DEFAULT=$DIR)";
  [ ! -z "$2" ] && REPO=$2 || REPO="alogentdigital" && repoDefaultMsg="(DEFAULT=$REPO)";
  print "INPUT => DIR=\"$1\" $dirDefaultMsg";
  print "INPUT => REPO=\"$2\" $repoDefaultMsg";
  print "PATH  => NXT_CODE_PATH/REPO/DIR/REPO";
  result=$NXT_CODE_PATH/$REPO/$DIR/$REPO
fi
printcmd "cd $result" && cd $result;
[ $? == 0 ] && pwd;
keyvalue set go_dir $result


shopt -s expand_aliases
source ~/.bash_profile
source $NXT_BASH_PATH/.nxt_bash
##########################
print '.'"$NXT_BASH_PATH"'/web.sh '"$@"

current_ip=$(myip) && current_ip=`echo $current_ip | sed 's/ *$//g'`;

WORKING_PATH=$(pwd) && print "WORKING_PATH=$WORKING_PATH";

[ -d "$WORKING_PATH/Alogent.Client.Web" ] && WebDir='Alogent.Client.Web' || WebDir='Alogent.Digital.User';
print "WebDir=$WORKING_PATH/$WebDir"

ng_port="4200"
ng_host="localhost"
# ng_host=$current_ip
ng_protocol="https"
# ng_protocol="http"

api_port="5001"
# api_port="5050"
api_host="localhost"
# api_host=$current_ip
api_protocol="https"
# api_protocol="http"

usage() { echo "usage:" && grep ".)\ #" $0; exit 0; }
[ $# -eq 0 ] && usage
while [ ! -z "$1" ]; do
  case "$1" in
-c | --clean        ) # Clean node_modules
  clean=true
;;
-i | --install      ) # Install node_modules
  install=true
;;
-b | --build        ) # Build 
  build=true
;;
-w | --watch-build  ) # Build and re-build on changes saved
   watch_build=true
;;   
-r | --run          ) # Run applications
  run=true
;;
-t | --test         ) # Run Angular Unit test
  test=true
;;
--ng-host           ) # host used by web applciato in; current ip
  shift
  ng_host=$1
  config=true
;;
--ng-port           ) # https port used by web applciato in; by default 4200
  shift
  ng_port=$1
  config=true
;;
--ng-protocol       ) # https protocol used by web applciato in; by default http
  shift
  ng_protocol=$1
  config=true
;;
--api-host          ) # Api host; current ip
  shift
  api_host=$1
  config=true
;;  
--api-port          ) # Api port; by default 5050
  shift
  api_port=$1
  config=true
;;
--api-protocol      ) # Api is using http/https, by default http
  shift
  api_protocol=$1
  config=true
;;
-h | --help         ) # Print help
  usage
  exit 0
;;
--config            ) # Applies developer environment config
  config=true
;;   
*                   )
  echo "Unkown argument $1"
  usage;
  exit 0
;;
  esac
shift
done

config() {
  destination="$WORKING_PATH/$WebDir/ClientApp/src/environments/environment.ts"
  template="$NXT_BASH_PATH/configs/base/environment.web.ts"
  modified="$NXT_BASH_PATH/configs/base/environment.web.temp.ts"

  printcmd "rm -f $modified"
  rm -f "$modified"

  printcmd "cp $template $modified"
  cp "$template" "$modified"

  if [[ "$api_host" = "localip" ]]
  then 
    api_host=$current_ip
  fi

  printcmd "sed -i '' -e s/{{IP_PLACE_HOLDER}}/$api_host/ $modified"
  sed -i '' -e "s/{{IP_PLACE_HOLDER}}/$api_host/" "$modified"

  printcmd "sed -i '' -e s/{{PORT_PLACE_HOLDER}}/$api_port/ $modified"
  sed -i '' -e "s/{{PORT_PLACE_HOLDER}}/$api_port/" "$modified"

  printcmd "sed -i '' -e s/{{PROTOCOL_PLACE_HOLDER}}/$api_protocol/ $modified"
  sed -i '' -e "s/{{PROTOCOL_PLACE_HOLDER}}/$api_protocol/" "$modified"

  printcmd "rm -f $destination"
  rm -f "$destination"

  printcmd "cp $temp $destination"
  cp "$modified" "$destination"
  
  printcmd "rm -f $modified"
  rm -f "$modified"
}

export TITLEPREFIX="WEB"

metadata_never_index() {
  if [[ "$OSTYPE" == "msys"* ]] || [[ "$OSTYPE" == "win"* ]]
  then #Windows
    notNeeded="On this OS metadata_never_index is not needed"
  else #Unix
    #on Mac machines, we need to skip this directories of being indexed by finder app
    cd $WORKING_PATH/$WebDir/ClientApp
  
    skip_indexing_dir="node_modules"
    [ ! -d $skip_indexing_dir ] && mkdir $skip_indexing_dir
    printcmd "touch ./$skip_indexing_dir/.metadata_never_index" 
    touch ./$skip_indexing_dir/.metadata_never_index
  fi
}

clean() {
  cd $WORKING_PATH/$WebDir/ClientApp
  metadata_never_index
  find ./ -type d  -path '*node_modules/*' -prune -o -type d -name 'node_modules' -exec touch '{}/.metadata_never_index' \;
  printcmd 'rm -rf node_modules'
  rm -rf node_modules
  printcmd 'rm -rf .angular'
  rm -rf .angular
  printcmd 'rm package-lock.json'
  [[ -f "package-lock.json" ]] && rm package-lock.json
  printcmd 'rm yarn.lock'
  [[ -f "yarn.lock" ]] && rm yarn.lock
  metadata_never_index
}

install() {
  cd $WORKING_PATH/$WebDir/ClientApp
  metadata_never_index
  printcmd 'npm i'
  npm i
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
  metadata_never_index
}

build() {
  cd $WORKING_PATH/$WebDir/ClientApp
  if [[ ! -d 'node_modules' ]]
  then  
    install
  fi
  printcmd 'npm run build'
  npm run build
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
}

watch_build() {
  cd $WORKING_PATH/$WebDir/ClientApp
  if [[ ! -d 'node_modules' ]]
  then  
    install
  fi
  printcmd "npm run -- ng build --watch true"
  npm run -- ng build --watch true
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
}

test() {
  cd $WORKING_PATH/$WebDir/ClientApp
  if [[ ! -d 'node_modules' ]]
  then  
    install
  fi
  # printcmd "npm run ng -- test --karma-config=karma.continuous.conf.js --sourceMap=false"
  # npm run ng -- test --karma-config=karma.continuous.conf.js --sourceMap=false
  printcmd "npm run test"
  npm run test
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
}

run() {

  if [[ "$ng_host" = "localip" ]]
  then 
    ng_host=$current_ip
  fi

  url="$ng_protocol://$ng_host:$ng_port"
  printcmd "browse $url"
  browse $url
  if [[ "$OSTYPE" != "msys"* ]] && [[ "$OSTYPE" != "win"* ]]
  then #Unix
    osascript -e 'tell application "Terminal" to activate';
  fi
  export TITLEPREFIX="WEB $url"
  cd $WORKING_PATH/$WebDir
  pwd
  cd ClientApp 
  if [[ $ng_protocol == "http" ]]
  then
    printcmd "ng serve --host $ng_host --port $ng_port "
    ng serve --host $ng_host --port $ng_port 
  else
    printcmd "ng serve --ssl --host $ng_host --port $ng_port "
    ng serve --ssl --host $ng_host --port $ng_port 
  fi
  
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
}

if [[ $config == true ]]
then
  config
fi

if [[ $clean == true ]]
then
  clean
fi

if [[ $install == true ]]
then
  install
fi

if [[ $watch_build == true ]] || [[ $build == true ]] || [[ $run == true ]] || [[ $test == true ]]
then

  if [[ $watch_build == true ]]
  then
    watch_build
  else

    if [[ $build == true ]]
    then
      build
    fi

    if [[ $test == true ]]
    then
      test
      exit 0;
    fi

    if [[ $run == true ]]
    then
      run
    fi
  fi
fi

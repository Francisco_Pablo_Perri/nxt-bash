shopt -s expand_aliases
source ~/.bash_profile
source $NXT_BASH_PATH/.nxt_bash
##########################
print '.'"$NXT_BASH_PATH"'/server.sh '"$@"

WORKING_PATH=$(pwd) && print "WORKING_PATH=$WORKING_PATH";

[ -d "$WORKING_PATH/Alogent.Client.Web" ] && WebDir='Alogent.Client.Web' || WebDir='Alogent.Digital.User';
print "WebDir=$WORKING_PATH/$WebDir"

[ -d "$WORKING_PATH/Alogent.Client.Admin" ] && AdminDir='Alogent.Client.Admin' || AdminDir='Alogent.Digital.Admin';
print "AdminDir=$WORKING_PATH/$AdminDir"

https_port=5001
http_port=5050

usage() { echo "usage:" && grep ".)\ #" $0; exit 0; }
[ $# -eq 0 ] && usage
while [ ! -z "$1" ]; do
  case "$1" in
-c | --clean        ) # Clean Build
  clean=true
;;
-b | --build        ) # Build 
  build=true
;;
-qb | --quick-build ) # Quick Build (swagger generation, node check and typesctipt)
  quickBuild=true
;;
-i | --install      ) # Restore Nuget Packages
  restore=true
;;
-r | --run          ) # Run User Server Api
  run=true;
;;
-t | --test         ) # Run Dotnet Unit test
  test=true
;;
--https             ) # https api port; by default 5001
  shift
  https_port=$1
;;  
--http              ) # https api port; by default 5050
  shift
  http_port=$1
;;
--clear-cache       ) # Clear Cache
  clear_cache=true
;;
--config            ) # Applies developer environment config
  config=true
;;
-h | --help         ) # Print help
  usage
  exit 0
;;
*                   )
  echo "Unkown argument $1"
  usage;
  exit 0
;;
  esac
shift
done

export TITLEPREFIX="SERVER"

if [[ $run == true ]]
then
  https_url="https://*:$https_port"
  print "https_url='$https_url'"

  http_url="http://*:$http_port"
  print "http_url='$http_url'"
fi

if [[ $clear_cache == true ]]
then
  current_ip=$(myip) && current_ip=`echo $current_ip | sed 's/ *$//g'`;

  printcmd "curl -v https://$current_ip:$https_port/api/Configurations/ClearCache"
  curl -v --insecure https://$current_ip:$https_port/api/Configurations/ClearCache
  exit 0;
fi

config() {
  #USER
  cd $WORKING_PATH/$WebDir

  printcmd 'rm -f appsettings.Local.json'
  rm -f appsettings.Local.json
  
  printcmd "cp $NXT_BASH_PATH/configs/base/config.appsettings.User.json ./appsettings.Local.json"
  cp $NXT_BASH_PATH/configs/base/config.appsettings.User.json ./appsettings.Local.json

  #ADMIN
  cd $WORKING_PATH/$AdminDir

  printcmd 'rm -f appsettings.Local.json'
  rm -f appsettings.Local.json

  printcmd 'cp $NXT_BASH_PATH/configs/base/config.appsettings.Admin.json ./appsettings.Local.json'
  cp $NXT_BASH_PATH/configs/base/config.appsettings.Admin.json ./appsettings.Local.json
}

metadata_never_index() {
  if [[ "$OSTYPE" != "msys"* ]] && [[ "$OSTYPE" != "win"* ]]
  then #Unix
    #on Mac machines, we need to skip this directories of being indexed by finder app

    skip_indexing_dir=$WORKING_PATH/$WebDir/bin
    [ ! -d $skip_indexing_dir ] && mkdir $skip_indexing_dir
    printcmd "touch $skip_indexing_dir/bin/.metadata_never_index" 
    touch $skip_indexing_dir/.metadata_never_index

    skip_indexing_dir=$WORKING_PATH/$WebDir/obj
    [ ! -d $skip_indexing_dir ] && mkdir $skip_indexing_dir
    printcmd "touch $skip_indexing_dir/bin/.metadata_never_index" 
    touch $skip_indexing_dir/.metadata_never_index

    skip_indexing_dir=$WORKING_PATH/$WebDir/ClientApp/node_modules
    [ ! -d $skip_indexing_dir ] && mkdir $skip_indexing_dir
    printcmd "touch $skip_indexing_dir/bin/.metadata_never_index" 
    touch $skip_indexing_dir/.metadata_never_index

    skip_indexing_dir=$WORKING_PATH/$WebDir/ClientApp/src/app/common/api
    [ ! -d $skip_indexing_dir ] && mkdir $skip_indexing_dir
    printcmd "touch $skip_indexing_dir/bin/.metadata_never_index" 
    touch $skip_indexing_dir/.metadata_never_index

    skip_indexing_dir=$WORKING_PATH/$AdminDir/bin
    [ ! -d $skip_indexing_dir ] && mkdir $skip_indexing_dir
    printcmd "touch $skip_indexing_dir/bin/.metadata_never_index" 
    touch $skip_indexing_dir/.metadata_never_index

    skip_indexing_dir=$WORKING_PATH/$AdminDir/obj
    [ ! -d $skip_indexing_dir ] && mkdir $skip_indexing_dir
    printcmd "touch $skip_indexing_dir/bin/.metadata_never_index" 
    touch $skip_indexing_dir/.metadata_never_index

    skip_indexing_dir=$WORKING_PATH/$AdminDir/ClientApp/node_modules
    [ ! -d $skip_indexing_dir ] && mkdir $skip_indexing_dir
    printcmd "touch $skip_indexing_dir/bin/.metadata_never_index" 
    touch $skip_indexing_dir/.metadata_never_index

    skip_indexing_dir=$WORKING_PATH/$AdminDir/ClientApp/src/app/common/api
    [ ! -d $skip_indexing_dir ] && mkdir $skip_indexing_dir
    printcmd "touch $skip_indexing_dir/bin/.metadata_never_index" 
    touch $skip_indexing_dir/.metadata_never_index
  fi
}

clean() {
  metadata_never_index
  cd $WORKING_PATH/
  printcmd 'dotnet clean'
  dotnet clean

  cd $WORKING_PATH/$WebDir
  printcmd 'rm -rf bin obj'
  rm -rf bin obj

  cd $WORKING_PATH/$AdminDir
  printcmd 'rm -rf bin obj'
  rm -rf bin obj

  metadata_never_index
}

restore() {
  cd $WORKING_PATH/
  printcmd 'dotnet restore'
  dotnet restore
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
}

quickBuild() {
  #USER
  cd $WORKING_PATH/$WebDir

  printcmd 'rm -f Alogent.Digital.User.csproj'
  rm -f Alogent.Digital.User.csproj
  
  printcmd "cp $NXT_BASH_PATH/configs/server-quick-build/Alogent.Digital.User.template ./Alogent.Digital.User.csproj"
  cp $NXT_BASH_PATH/configs/server-quick-build/Alogent.Digital.User.template ./Alogent.Digital.User.csproj

  #ADMIN
  cd $WORKING_PATH/$AdminDir

  printcmd 'rm -f Alogent.Digital.Admin.csproj'
  rm -f Alogent.Digital.Admin.csproj
  
  printcmd "cp $NXT_BASH_PATH/configs/server-quick-build/Alogent.Digital.Admin.template ./Alogent.Digital.Admin.csproj"
  cp $NXT_BASH_PATH/configs/server-quick-build/Alogent.Digital.Admin.template ./Alogent.Digital.Admin.csproj
  
  cd $WORKING_PATH/Weavers
  printcmd 'dotnet build'
  dotnet build
  cd $WORKING_PATH/
  printcmd 'dotnet build'
  dotnet build
  git checkout $WORKING_PATH/$WebDir/Alogent.Digital.User.csproj
  git checkout $WORKING_PATH/$AdminDir/Alogent.Digital.Admin.csproj
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
  cleanNSwagFilesAfterBuild "$WORKING_PATH/$WebDir/ClientApp/src/app/common/api"
  cleanNSwagFilesAfterBuild "$WORKING_PATH/$AdminDir/ClientApp/src/app/common/api"
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
}

build() {
  cd $WORKING_PATH/Weavers
  printcmd 'dotnet build'
  dotnet build
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
  cd $WORKING_PATH/
  printcmd 'dotnet build'
  dotnet build
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
  cleanNSwagFilesAfterBuild "$WORKING_PATH/$WebDir/ClientApp/src/app/common/api"
  cleanNSwagFilesAfterBuild "$WORKING_PATH/$AdminDir/ClientApp/src/app/common/api"
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
}

cleanNSwagFilesAfterBuild() {
  dirToVerify=$1
  cd $dirToVerify
  # print "Verifying files under $dirToVerify"
  for OUTPUT in $(ls -R . | awk '/:$/&&f{s=$0;f=0}/:$/&&!f{sub(/:$/,"");s=$0;f=1;next}NF&&f{ print s"/"$0 }')
  do
    ext="${OUTPUT##*.}"
    if [[ ! -s $OUTPUT ]]; then
      if [[ $ext == ts ]]; then
        # The file is empty.
        print "EMPTY FILE FOUND $OUTPUT"
        printcmd "git checkout -- $OUTPUT"
        git checkout -- $OUTPUT
      fi
    fi
  done
}

test() {
  cd $WORKING_PATH
  printcmd "dotnet test"
  dotnet test
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
}

run() {
  proj=$WebDir
  cd $WORKING_PATH/$proj
  pwd
  
  # cmd=".\bin\Debug\netcoreapp3.1\Alogent.Digital.User.dll"
  cmd=".\bin\Debug\net6.0\Alogent.Digital.User.dll"
  if [[ ! -f "$cmd" ]]; then
    cmd="run"
  fi
  export TITLEPREFIX="SERVER $http_url;$https_url"
  if [[ $https_port ]] && [[ $http_port ]] 
  then
    printcmd "dotnet $cmd --urls "$http_url";"$https_url" --no-build"
    dotnet $cmd --urls "$http_url;$https_url" --no-build
    [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
  elif [[ $https_port ]] && [[ -z $http_port ]] 
  then
    printcmd "dotnet $cmd --urls "$https_url" --no-build"
    dotnet $cmd --urls "$https_url" --no-build
    [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
  elif [[ -z $https_port ]] && [[ $http_port ]] 
  then
    printcmd "dotnet $cmd --urls "$http_url" --no-build"
    dotnet $cmd --urls "$http_url" --no-build
    [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
  else
    printcmd "dotnet $cmd --no-build"
    dotnet $cmd --no-build
    [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
  fi
}

if [[ $config == true ]]
then
  config
fi

if [[ $clean == true ]]
then
  clean
fi

if [[ $restore == true ]]
then
  restore
fi

if [[ $quickBuild == true ]]
then
  quickBuild
elif [[ $build == true ]]
then
   build
fi

if [[ $run == true ]]
then
  run
fi

shopt -s expand_aliases
source ~/.bash_profile
source $NXT_BASH_PATH/.nxt_bash
##########################

color=35;

usage() { 
    echo "\$1 text; \$2 color" && grep ".)\ #" $0; 
    exit 0; 
}
[[ "$1" == "-h" ]] && usage
[[ "$1" == "--help" ]] && usage
case $2 in 
gray                ) # gray            
    color=30;
;;
red                 ) # red            
    color=31;
;;
green               ) # green          
    color=32;
;;
yellow              ) # yellow              
    color=33;
;;
blue                ) # blue           
    color=34;
;;
purple              ) # purple         
    color=35;
;;
cyan                ) # cyan           
    color=36;
;;
white               ) # white                
    color=37;
;;
-h | --help         ) # Print help
    usage
    exit 0
;;
esac

dir=$(pwd)
txt="cd $dir && $1"
echo -e "\033[$color""m :: [NXT-BASH] :: Executing :: $txt\033[0m";

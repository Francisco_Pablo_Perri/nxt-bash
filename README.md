# README

CLI to help run NXT BASE quickly during development

### Prerequisites?

You might need to check that you dev environment is setup properly before attempting to use nxt-bash:

- [Getting Started - Application & Web](https://bitbucket.org/jwaala/alogentdigital/wiki/Getting%20Started%20-%20Application%20&%20Web.md)
- [Getting Started - Mobile](https://bitbucket.org/jwaala/alogentdigital/wiki/Getting%20Started%20-%20Mobile.md)
- [Running NXT on a Mac](https://bitbucket.org/jwaala/alogentdigital/wiki/Mobile%20-%20Developer%20-%20Running%20NXT%20on%20a%20Mac.md)
- [SQL server on a Mac](https://bitbucket.org/jwaala/alogentdigital/wiki/Mobile%20-%20Developer%20-%20SQL%20Server%20on%20a%20Mac.md)

### How do I get it set up?

- You should bash terminal to use this scripts
- Bash terminal should recognize `code` as vscode
- Place the file `.nxt_bash` on your `~/` directory.
  - On MAC you will find it on `/Users/{{YOUR_USER_NAME}}/`
  - On WINDOWS you will find it on `/C/Users/{{YOUR_USER_NAME}}/`
- In the file `.nxt_bash` replace the line that defines the git user:

- Run the following on bash:

```
cd ~ && bash .nxt_bash
```

### How do I use it?

- Start by running `nxt clone --branch master --dir master`
  - You can run task for the the Server API with
    - `nxt server --config`
    - `nxt server --clean`
    - `nxt server --install`
    - `nxt server --build`
    - `nxt server --run`
    - `nxt server --config --clean --install --build --run`
  - You can run task for the the User Angular Web App with
    - `nxt web --config`
    - `nxt web --clean`
    - `nxt web --install`
    - `nxt web --build`
    - `nxt web --run`
    - `nxt web --config --clean --install --build --run`
  - You can run task for the the Admin Angular Web App with
    - `nxt admin --config`
    - `nxt admin --clean`
    - `nxt admin --install`
    - `nxt admin --build`
    - `nxt admin --run`
    - `nxt admin --config --clean --install --build --run`
  - You can run task for the the Mobile Nativescript App with
    - `nxt mobile --config`
    - `nxt mobile --clean`
    - `nxt mobile --install`
    - `nxt mobile [NATIVESCRIPT_COMMANDS]`
    - `nxt mobile build android`
    - `nxt mobile run ios`
    - `nxt mobile --config --clean --install --build run ios`
    - `nxt mobile --config --clean --install --build [NATIVESCRIPT_COMMANDS]`
- Run for more help about how to use it:
  - `nxt --help`
  - `nxt server --help`
  - `nxt web --help`
  - `nxt admin --help`
  - `nxt mobile --help`

### Contribution guidelines

- This repos is read-only at the time

### Who do I talk to?

- Repo owner or admin - Francisco Pablo Perri
- Other community or team contact

### Repo

- https://bitbucket.org/Francisco_Pablo_Perri/nxt-bash/src/master/

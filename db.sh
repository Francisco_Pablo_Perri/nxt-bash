shopt -s expand_aliases
source ~/.bash_profile
source $NXT_BASH_PATH/.nxt_bash
##########################
print '.'"$NXT_BASH_PATH"'/db.sh '"$@"

WORKING_PATH=$(pwd) && print "WORKING_PATH=$WORKING_PATH";

container_name='sql_server_2017_3'
sa_password='reallyStrongPwd123'

usage() { echo "usage:" && grep ".)\ #" $0; exit 0; }
[ $# -eq 0 ] && usage
while [ ! -z "$1" ]; do
  case "$1" in
--migration         ) # EF migration  
  if [[ -z $2 ]]
  then
    print "the argument '--migration' should be followed by a migration name"
    exit 99;
  elif [[ $2 == --* ]]
  then
    print "migration name cannot start with '--'"
    exit 99;
  elif [[ $2 == -* ]]
  then
    print "migration name cannot start with '-'"
    exit 99;
  else
    migration=$2
    print '$migration="'$migration'"'
    shift
  fi
;;  
-i | --initialize-container   ) # Initializes fresh docker container with empty AlogentDigital DB
  initialize=true
;;  
-c | --container              ) # Docker Container Name
  shift
  container_name=$1
;;
-h | --help                   ) # Print help
  usage
  exit 0
;;
*                             )
  echo "Unkown argument $1"
  usage;
  exit 0
;;
  esac
shift
done

migration() {
  cd $WORKING_PATH/Alogent.Digital.Data

  printcmd "dotnet ef migrations add $migration --project ../Alogent.Digital.Data.Migrations/Alogent.Digital.Data.Migrations.csproj --startup-project ../Alogent.Digital.User/Alogent.Digital.User.csproj --no-build"
  dotnet ef migrations add $migration --project ../Alogent.Digital.Data.Migrations/Alogent.Digital.Data.Migrations.csproj --startup-project ../Alogent.Digital.User/Alogent.Digital.User.csproj --no-build
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;

  cd $WORKING_PATH
  printcmd "dotnet build"
  dotnet build
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
 
  cd $WORKING_PATH
  printcmd "pwsh $WORKING_PATH/Scripts/generate-db-scripts.ps1"
  pwsh $WORKING_PATH/Scripts/generate-db-scripts.ps1
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
}

undo_migration() {
  cd $WORKING_PATH/Alogent.Digital.Data
  printcmd "dotnet ef migrations remove --project ../Alogent.Digital.Data.Migrations/Alogent.Digital.Data.Migrations.csproj --startup-project ../Alogent.Digital.User/Alogent.Digital.User.csproj --no-build"
  dotnet ef migrations remove --project ../Alogent.Digital.Data.Migrations/Alogent.Digital.Data.Migrations.csproj --startup-project ../Alogent.Digital.User/Alogent.Digital.User.csproj --no-build
  [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
}

initialize() {
  initilize_Script=`cat $NXT_BASH_PATH/configs/base/initialize.db.sql` #this creates a new AlogentDigital DB 

  printcmd "docker run -d --name $container_name -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=$sa_password" -p 1433:1433 mcr.microsoft.com/mssql/server:2022-latest"
  docker run -d --name $container_name -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=$sa_password" -p 1433:1433 mcr.microsoft.com/mssql/server:2022-latest

  printcmd "exec -it $container_name /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P $sa_password -Q $initilize_Script"
  docker exec -it $container_name /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P $sa_password -Q "$initilize_Script"
}

cmd() {
  # cmd is whatever sql command you want to execute inside of the container
  docker exec -it $container_name /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P $sa_password -Q "$cmd"
}

if [[ ! -z $migration ]]
then
  migration
fi

if [[ ! -z $initialize ]]
then
  initialize
fi

USE master;
IF (EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE ('[' + name + ']' = 'AlogentDigital' OR name = 'AlogentDigital')))
BEGIN
    DROP DATABASE AlogentDigital
END
GO
CREATE DATABASE AlogentDigital
If not Exists (select loginname from master.dbo.syslogins 
    where name = 'AlogentDigital')
Begin
    CREATE LOGIN AlogentDigital 
    WITH PASSWORD = 'Al0g3nt!'; 
End
GO
USE master;
GRANT CONNECT SQL TO AlogentDigital;
GRANT CREATE ANY DATABASE TO AlogentDigital;
GO
USE AlogentDigital;
GO
CREATE USER AlogentDigital FOR LOGIN AlogentDigital;
GO
USE AlogentDigital;
GO
ALTER ROLE db_owner ADD MEMBER AlogentDigital;
GO


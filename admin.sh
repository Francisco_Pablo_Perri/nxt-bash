shopt -s expand_aliases
source ~/.bash_profile
source $NXT_BASH_PATH/.nxt_bash
##########################
print '.'"$NXT_BASH_PATH"'/admin.sh '"$@"

WORKING_PATH=$(pwd) && print "WORKING_PATH=$WORKING_PATH";

[ -d "$WORKING_PATH/Alogent.Client.Admin" ] && AdminDir='Alogent.Client.Admin' || AdminDir='Alogent.Digital.Admin';
print "AdminDir=$WORKING_PATH/$AdminDir"

https_port=5051
http_port=44337

usage() { echo "usage:" && grep ".)\ #" $0; exit 0; }
[ $# -eq 0 ] && usage
while [ ! -z "$1" ]; do
  case "$1" in
-c | --clean        ) # Clean node_modules
  clean=true
;;
-i | --install      ) # Install node_modules
  install=true
;;
-b | --build        ) # Build 
  build=true
;;
-w | --watch-build  ) # Build and re-build on changes saved
   watch_build=true
;;   
-r | --run          ) # Run applications
  run=true
;;
--https             ) # https api port; by default 5001
  shift
  config=true
  https_port=$1
;;  
--http              ) # https api port; by default 5000
  shift
  config=true
  http=true
  http_port=$1
;;
--clear-cache       ) # Clear Cache
  clear_cache=true
;;
--config            ) # Applies developer environment config
  config=true
;;
-h | --help         ) # Print help
  usage
  exit 0
;;
*                   )
  echo "Unkown argument $1"
  usage;
  exit 0
;;
  esac
shift
done

export TITLEPREFIX="ADMIN"

if [[ $clear_cache == true ]] || [[ $run == true ]]
then
  https_url="https://localhost:$https_port"
  print "https_url='$https_url'"
  
  [[ -z $ng_api ]] && ng_api="$https_url"

  if [[ $http == true ]]
  then
    http_url="http://*:$http_port"
    print "http_url='$http_url'"
  fi
fi

config() {
  cd $WORKING_PATH/$AdminDir

  printcmd 'rm -f appsettings.Local.json'
  rm -f appsettings.Local.json

  printcmd 'cp $NXT_BASH_PATH/configs/base/config.appsettings.Admin.json ./appsettings.Local.json'
  cp $NXT_BASH_PATH/configs/base/config.appsettings.Admin.json ./appsettings.Local.json
}

metadata_never_index() {
  if [[ "$OSTYPE" == "msys"* ]] || [[ "$OSTYPE" == "win"* ]]
  then #Windows
    notNeeded="On this OS metadata_never_index is not needed"
  else #Unix
    #on Mac machines, we need to skip this directories of being indexed by finder app
    cd $WORKING_PATH/$AdminDir/ClientApp
  
    skip_indexing_dir="node_modules"
    [ ! -d $skip_indexing_dir ] && mkdir $skip_indexing_dir
    printcmd "touch ./$skip_indexing_dir/.metadata_never_index" 
    touch ./$skip_indexing_dir/.metadata_never_index
  fi
}

clean() {
  cd $WORKING_PATH/$AdminDir/ClientApp
  metadata_never_index
  find ./ -type d  -path '*node_modules/*' -prune -o -type d -name 'node_modules' -exec touch '{}/.metadata_never_index' \;
  printcmd 'rm -rf node_modules'
  rm -rf node_modules
  printcmd 'rm -rf .angular'
  rm -rf .angular
  printcmd 'rm package-lock.json'
  [[ -f "package-lock.json" ]] && rm package-lock.json
  printcmd 'rm yarn.lock'
  [[ -f "yarn.lock" ]] && rm yarn.lock
  metadata_never_index
}

install() {
  cd $WORKING_PATH/$AdminDir/ClientApp
  metadata_never_index
  printcmd 'npm i'
  npm i
}

build() {
  cd $WORKING_PATH/$AdminDir/ClientApp
  if [[ ! -d 'node_modules' ]]
  then  
    install
  fi
  printcmd 'npm run build'
  npm run build
}

watch_build() {
  cd $WORKING_PATH/$AdminDir/ClientApp
  if [[ ! -d 'node_modules' ]]
  then  
    install
  fi
  printcmd "npm run -- ng build --watch true"
  npm run -- ng build --watch true
}

run() {
  printcmd "browse $https_url"
  browse $https_url
  if [[ "$OSTYPE" != "msys"* ]] && [[ "$OSTYPE" != "win"* ]]
  then #Unix
    osascript -e 'tell application "Terminal" to activate';   
  fi
  export TITLEPREFIX="ADMIN $https_url" 
  cd $WORKING_PATH/$AdminDir

  # cmd=".\bin\Debug\netcoreapp3.1\Alogent.Digital.Admin.dll"
  cmd=".\bin\Debug\net6.0\Alogent.Digital.Admin.dll"
  if [[ ! -f "$cmd" ]]; then
    cmd="run"
  fi
  if [[ $ng_only ]]
  then
    cd ClientApp 
    npm run start
  elif [[ -z $http_url ]]
  then
    printcmd "dotnet $cmd --urls $https_url --no-build"
    dotnet $cmd --urls "$https_url" --no-build
  else
    printcmd "dotnet $cmd --urls $http_url;$https_url --no-build"
    dotnet $cmd --urls "$http_url;$https_url" --no-build
  fi
}

if [[ $config == true ]]
then
  config
fi

if [[ $clear_cache == true ]]
then
  printcmd "browse $https_url/api/Configurations/ClearCache"
  browse "$https_url/api/Configurations/ClearCache"
  if [[ "$OSTYPE" != "msys"* ]] && [[ "$OSTYPE" != "win"* ]]
  then #Unix
    osascript -e 'tell application "Terminal" to activate';
  fi
  exit 0;
fi

if [[ $clean == true ]]
then
  clean
fi

if [[ $install == true ]]
then
  install
fi

if [[ $watch_build == true ]] || [[ $build == true ]] || [[ $run == true ]]
then

  if [[ $watch_build == true ]]
  then
    watch_build
  else

    if [[ $build == true ]]
    then
      build
    fi

    if [[ $run == true ]]
    then
      run
    fi
  fi
fi

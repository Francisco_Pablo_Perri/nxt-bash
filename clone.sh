shopt -s expand_aliases
source ~/.bash_profile
source $NXT_BASH_PATH/.nxt_bash
##########################
print '.'"$NXT_BASH_PATH"'/clone.sh '"$@"

if [[ -z $NXT_BASH_PATH ]] || [[ -z $NXT_CODE_PATH ]]
then
    print "These enviroment variables are required:"
    print "\$NXT_BASH_PATH='"$NXT_BASH_PATH"'               #folder where nxt bash scritps are."
    print "\$NXT_CODE_PATH='"$NXT_CODE_PATH"'               #folder where git repositories are created."
fi

usage() { echo "usage:" && grep ".)\ #" $0; exit 0; }
[ $# -eq 0 ] && usage
while [ ! -z "$1" ]; do
  case "$1" in
--branch            ) # Branch; by default "master"
  shift
  branch=$1
;;
--dir               ) # Repository folder; by default "master"
  shift
  dir=$1
;;   
--client | --repo   ) # Repository Name; by default "alogentdigital"
  shift
  repo=$1
;;
--init-server       ) # Initialize Dotnet API Server
  server=true
;;
--init-web          ) # Initialize Web Angular application
  web=true
;;
--init-admin        ) # Initialize Admin Angular application
  admin=true
;;
--init-mobile       ) # Initialize Mobile Nativescript application
  mobile=true
;;
-h | --help         ) # Print help
  usage
  exit 0
;;
*                   )
  echo "Unkown argument $1"
  usage;
  exit 0
;;
  esac
shift
done

makeDir() {
  [ ! -d $1  ] && { printcmd "mkdir $1";mkdir $1; }
}

removeDir() {
  [ -d $1  ] && { printcmd "rmdir $1";rmdir $1; }
}

initDir() {
  if [[ -d $1  ]]
  then
    print "Directory $1 Exists";
    if [[ -z "$(ls -A $1)" ]]
    then
      print "Directory $1 is Empty";
      removeDir $1
      makeDir $1
    else
      print "Directory $1 is Not Empty";
    fi
  else
     print "Directory $1 Does Not Exists";
     makeDir $1
  fi
}

[[ -z $dir ]] && dir='master'
print "dir='$dir'"

[[ -z $branch ]] && branch='master'
print "branch='$branch'"

[[ -z $repo ]] && repo='alogentdigital'
print "repo='$repo'"

cd $NXT_CODE_PATH;
initDir $repo;
WORKING_FOLDER="$NXT_CODE_PATH/$repo/";
cd $WORKING_FOLDER
initDir $dir
cd $dir

export NXT_CLONE_REPO="https://bitbucket.org/jwaala/$repo.git"; # ALOGENT DIGITAL BASE PRODUCT

printcmd "git clone $NXT_CLONE_REPO" && git clone $NXT_CLONE_REPO;

cd $repo
printcmd "git checkout $branch" && git checkout $branch;
printcmd "git branch" && git branch

result="$WORKING_FOLDER/$dir/$repo"
printcmd "cd $result"  && cd $result;
keyvalue set redirect $result

if [[ $server == true ]] || [[ $web == true ]] || [[ $admin == true ]] || [[ $mobile == true ]]
then
  nxt server --clean --install
  nxt web --clean --install
  nxt admin --clean --install

  nxt server --build
fi

[[ $web == true ]] &&  nxt web --build;
[[ $admin == true ]] &&  nxt admin --build;
[[ $mobile == true ]] && nxt mobile --clean --install

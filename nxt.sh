shopt -s expand_aliases
source ~/.bash_profile
source $NXT_BASH_PATH/.nxt_bash
##########################
print '.'"$NXT_BASH_PATH"'/nxt.sh '"$@"

log() {  sh  $NXT_BASH_PATH/print.sh "$1" yellow; }
if [[ -z $NXT_BASH_PATH ]] || [[ -z $NXT_CODE_PATH ]]
then
    log "These enviroment variables are required:"
    log "\$NXT_BASH_PATH='"$NXT_BASH_PATH"'   #folder where this nxt bash scritps is"
    log "\$NXT_CODE_PATH='"$NXT_CODE_PATH"'   #folder where git repositories are created"
fi
export TITLEPREFIX="NXT"
metadata_never_index() {
    if [[ "$OSTYPE" != "msys"* ]] && [[ "$OSTYPE" != "win"* ]]
    then #Unix
        #on Mac machines, we need to skip this directories of being indexed by finder app
        find ./ -type d  -path '*node_modules/*' -prune -o -type d -name 'node_modules' -exec touch '{}/.metadata_never_index' \;

        find ./ -type d  -path '*hooks/*' -prune -o -type d -name 'hooks' -exec touch '{}/.metadata_never_index' \;

        find ./ -type d  -path '*platforms/*' -prune -o -type d -name 'platforms' -exec touch '{}/.metadata_never_index' \;

        find ./ -type d  -path '*bin/*' -prune -o -type d -name 'bin' -exec touch '{}/.metadata_never_index' \;

        find ./ -type d  -path '*obj/*' -prune -o -type d -name 'obj' -exec touch '{}/.metadata_never_index' \;
    fi
}

usage() { echo "usage:" && grep ".)\ #" $0; exit 0; }
[ $# -eq 0 ] && usage
while [ ! -z "$1" ]; do
  case "$1" in
clone               ) # Clone
    shift;
     sh  $NXT_BASH_PATH/clone.sh $@;
    exit 0;
;;  
admin               ) # Cli for Admin WebSite Angular
    shift;
     sh  $NXT_BASH_PATH/admin.sh $@;
    exit 0;
;;  
web                 ) # Cli for User WebSite Angular
    shift;
     sh  $NXT_BASH_PATH/web.sh $@;
    exit 0;
;;
server              ) # Cli for C# Solution
    shift;
     sh  $NXT_BASH_PATH/server.sh $@;
    exit 0;
;;
mobile              ) # Cli for Mobile Nativescript/Angular
    shift;
     sh  $NXT_BASH_PATH/mobile.sh $@;
    exit 0;
;;
db                  ) # Cli for DB administration
    shift;
    sh  $NXT_BASH_PATH/db.sh $@;
    exit 0;
;;
edit                ) # Edit Nxt Scripts
    shift;
    code $NXT_BASH_PATH/bash.code-workspace;
    exit 0;
;;
-cb | --clean-build ) # Print Help
   sh $NXT_BASH_PATH/web.sh -c -i $@       && [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
   sh $NXT_BASH_PATH/admin.sh -c -i $@     && [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
   sh $NXT_BASH_PATH/server.sh -c -i -b $@ && [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
   sh $NXT_BASH_PATH/web.sh -b $@          && [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
   sh $NXT_BASH_PATH/admin.sh -b $@        && [[ ! $? == 0 ]] && print "exited with code $?" && exit $?;
   exit 0;
;;
-h | --help         ) # Print Help
   usage;
   exit 0;
;;
*                   )
  echo "Unkown argument $1";
  usage;
  exit 0;
;;
 esac
shift
done

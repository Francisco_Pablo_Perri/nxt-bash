-- SymXchange Sandbox QA User Pre-load
DECLARE @passwordHash nvarchar(max) = 'AQAAAAEAACcQAAAAECM60Wy1KRg75FyElV2vnDblRzkP7vH0viqqgKf1Pmywwl5723nvgXPcD7ognrsYEw=='
CREATE TABLE #TempUsers
(
	[Id] int,
	[Name] VARCHAR(60),
	[Ssn] VARCHAR(10),
	[Hash] VARCHAR(max)
)
INSERT INTO #TempUsers (Id, [Name], Ssn, [Hash])
    SELECT 1, 'Keller', '007150411', '1a54b17a983f19738f1c9496060ade4a4609a8a389945265085d0e9968dcebb7' UNION
	SELECT 2, 'Mccullough', '982822202', 'f0b4172567832874824fcff8be956a8f0687c08f4d92904ba38622ed81f0603a'
DECLARE @i int = 0;
DECLARE @count int = (SELECT COUNT(*) FROM #TempUsers);
DECLARE @portfolioId bigint;
DECLARE @userId nvarchar(100);
DECLARE @userName nvarchar(64);
DECLARE @ssn nvarchar(10);
DECLARE @hashString nvarchar(max);
WHILE @i < @count
BEGIN
     SELECT @i = @i + 1
	 DECLARE @guid uniqueidentifier  
	 SET @guid = NEWID() 
	 SET @userId = LOWER(CONVERT(varchar(255), @guid))
	 SET @username = 'test'+(SELECT [Name] FROM #TempUsers WHERE Id = @i);
	 SET @ssn = (SELECT [Ssn] FROM #TempUsers WHERE Id = @i);
	 SET @hashString = (SELECT [Hash] FROM #TempUsers WHERE Id = @i);
	 IF (EXISTS (SELECT * FROM DigitalUsers WHERE NormalizedUserName = UPPER(@username)))
	 BEGIN
		DECLARE @existingPortfolioId bigint = (SELECT PortfolioId FROM DigitalUsers WHERE NormalizedUserName = UPPER(@username));
		UPDATE Portfolios
			SET IdentifierHash = @hashString
		WHERE Id = @existingPortfolioId;
	 END
	 ELSE
	 BEGIN
		 INSERT INTO Portfolios (InsertAt, UpdateAt, Identifier, IdentifierHash)
		 VALUES	(GETUTCDATE(), GETUTCDATE(), @ssn, LOWER(@hashString));
		 SET @portfolioId = SCOPE_IDENTITY();
		 INSERT INTO DigitalUsers (InsertAt, UpdateAt, Id, Type, PasswordHash, UserName, NormalizedUserName, PortfolioId, Status, ConcurrencyStamp, PasscodeFailedCount, PasscodeFailedCountTotal, AccessFailedCount, AccessFailedCountTotal, AccessSucceededCount, AccessSucceededCountTotal, PasswordChangedUtc)
		 VALUES (GETUTCDATE(), GETUTCDATE(), @userId, 0, @passwordHash, @username, UPPER(@username), @portfolioId, 0, CONVERT(varchar(255), NEWID()), 0, 0, 0, 0, 0, 0, GETUTCDATE());
		 UPDATE Portfolios
		 SET OwnerUserId = @userId
		 WHERE Id = @portfolioId;
	 END
END
DROP TABLE #TempUsers
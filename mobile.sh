shopt -s expand_aliases
source ~/.bash_profile
source $NXT_BASH_PATH/.nxt_bash
##########################
print '.'"$NXT_BASH_PATH"'/mobile.sh '"$params"

current_ip=$(myip) && current_ip=`echo $current_ip | sed 's/ *$//g'`;

WORKING_PATH=$(pwd) && print "WORKING_PATH=$WORKING_PATH";

[ -d "$WORKING_PATH/Alogent.Client.Mobile" ] && MobileDir='Alogent.Client.Mobile' || MobileDir='Alogent.Digital.Mobile';
print "MobileDir=$WORKING_PATH/$MobileDir"
cd "$WORKING_PATH/$MobileDir"

# api_protocol="https"
api_protocol="http"
# api_host="localhost"
api_host=$current_ip
# api_port="5001"
api_port="5050"

usage() { 
  echo "";
  echo "usage:";
  echo "nxt mobile <Action> <Platform> [--clean-modules] [--install] [--api]";
  echo "";
  echo "usage:" && grep ".)\ #" $0; 
  echo "";
  exit 0; 
}
[ $# -eq 0 ] && usage
while [ ! -z "$1" ]; do
  case "$1" in
-d | --diagnostic           ) # Quick Version diagnostinc
    nxtDiagnostic=true
;;
--init                      ) # Initializes the CLI to make sure you are running correct version for this branch (Node, NPM, NG, NS, etc)
    init=true
;;
-c | --clean-modules        ) # Clean node_modules
  clean_modules=true
;;
-hc | --hard-clean-modules  ) # Hard Clean node_modules
  hard_clean_modules=true
;;
-i | --install              ) # Install node_modules
  install=true
;;
--api-host                  ) # Api host; by default localhost ip
  shift
  api_host=$1
  config=true
;;    
--api-port                  ) # Api http port; by default 5050
  shift
  api_port=$1
  config=true
;;
--api-protocol              ) # Api is using http/https, by default https
  shift
  api_protocol=$1
  config=true
;;  
--config                    ) # Applies development environment configuration
  config=true
;;  
-h | --help                 ) # Print help
  usage
  exit 0
;;
*                           )
  tnsCommands="$tnsCommands"' '"$1"
;;
  esac
  params="$params"' '"$1"
shift
done

export TITLEPREFIX="MOBILE"
print "tnsCommands= 'tns $tnsCommands'"

config() {
  destination=""$WORKING_PATH/$MobileDir"/src/app/config/config.json"
  template="$NXT_BASH_PATH/configs/base/config.mobile.json"
  modified="$NXT_BASH_PATH/configs/base/config.mobile.temp.json"

  printcmd "rm -f $modified"
  rm -f "$modified"

  printcmd "cp $template $modified"
  cp "$template" "$modified"

    if [[ "$api_host" = "localip" ]]
  then 
    api_host=$current_ip
  fi

  printcmd "sed -i '' -e s/{{IP_PLACE_HOLDER}}/$api_host/ $modified"
  sed -i '' -e "s/{{IP_PLACE_HOLDER}}/$api_host/" "$modified"

  printcmd "sed -i '' -e s/{{PORT_PLACE_HOLDER}}/$api_port/ $modified"
  sed -i '' -e "s/{{PORT_PLACE_HOLDER}}/$api_port/" "$modified"

  printcmd "sed -i '' -e s/{{PROTOCOL_PLACE_HOLDER}}/$api_protocol/ $modified"
  sed -i '' -e "s/{{PROTOCOL_PLACE_HOLDER}}/$api_protocol/" "$modified"

  printcmd "rm -f $destination"
  rm -f "$destination"

  printcmd "cp $temp $destination"
  cp "$modified" "$destination"
  
  printcmd "rm -f $modified"
  rm -f "$modified"
}

metadata_never_index() {
    cd "$WORKING_PATH/$MobileDir"
    if [[ "$OSTYPE" != "msys"* ]] && [[ "$OSTYPE" != "win"* ]]
    then #Unix
        #on Mac machines, we need to skip this directories of being indexed by finder app
        # printcmd "find ./ -type d  -path '*node_modules/*' -prune -o -type d -name 'node_modules' -exec touch '{}/.metadata_never_index' \;"
        find ./ -type d  -path '*node_modules/*' -prune -o -type d -name 'node_modules' -exec touch '{}/.metadata_never_index' \;
    fi
}

clean() {
  cd "$WORKING_PATH/$MobileDir"
  metadata_never_index
  printcmd "tns clean"
  tns clean
  metadata_never_index
}

hardClean() {
  cd "$WORKING_PATH/$MobileDir"
  metadata_never_index
    print "tns clean: failed.... removing directories manually"
    metadata_never_index
    printcmd 'rm -rf node_modules'
    rm -rf node_modules
    printcmd 'rm -rf hooks'
    rm -rf hooks
    printcmd 'rm -rf platforms'
    rm -rf platforms
    metadata_never_index
    printcmd 'tns platform remove android'
    tns platform remove android
    printcmd 'tns platform remove ios'
    tns platform remove ios
    metadata_never_index
    clean
  # printcmd 'git restore package-lock.json'
  # git restore package-lock.json
}

install() {
    cd "$WORKING_PATH/$MobileDir"
    mkdir node_modules
    metadata_never_index
    cd "$WORKING_PATH/$MobileDir"
    printcmd 'npm i'
    npm i
    metadata_never_index
}


if [[ $nxtDiagnostic == true ]]
then
  cd "$WORKING_PATH/$MobileDir"
  bash ./scripts/TeamCity/nxt-mobile.sh --diagnostic
fi

if [[ $init == true ]]
then
  cd "$WORKING_PATH/$MobileDir"
  bash ./scripts/TeamCity/nxt-mobile.sh --init
fi

if [[ $config == true ]]
then
  config
fi

if [[ $hard_clean_modules == true ]]
then
  hardClean
elif [[ $clean_modules == true ]]
then
  clean
fi

cd "$WORKING_PATH/$MobileDir"
if [[ $install == true ]]
then  
  install
fi

cd "$WORKING_PATH/$MobileDir"


if [[ $tnsCommands ]]
then
  export TITLEPREFIX="MOBILE $tnsCommands"
  if [[ $clean_modules == true ]]
  then
      printcmd 'tns'"$tnsCommands"' --clean' 
      tns $tnsCommands
  else
      printcmd 'tns'"$tnsCommands"
      tns $tnsCommands
  fi
fi

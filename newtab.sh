shopt -s expand_aliases
source ~/.bash_profile
source $NXT_BASH_PATH/.nxt_bash
##########################
print '.'"$NXT_BASH_PATH"'/newtb.sh '"$@"

log() { sh $NXT_BASH_PATH/print.sh "$1" yellow; }
if [[ -z $NXT_BASH_PATH ]] || [[ -z $NXT_BASE_CODE_PATH ]]
then
    log "These enviroment variables are required:"
    log "\$NXT_BASH_PATH='"$NXT_BASH_PATH"'   #folder where this nxt bash scritps is"
    log "\$NXT_BASE_CODE_PATH='"$NXT_BASE_CODE_PATH"'   #folder where git repositories are created"
fi

# privatett() {
#   print 'new tab =>'"$1"
#   osascript 
#   -e 'tell application "Terminal" to activate' \
#   -e 'tell application "System Events" to tell process "Terminal" to keystroke "t" using command down' \
#   -e 'tell application "System Events" to tell process "Terminal" to keystroke "'"$instruction"'"' \
#   -e 'tell application "System Events" to tell process "Terminal" to key code 52' \
#   -e 'tell application "System Events" to tell process "Terminal" to keystroke "exit"' \
#   -e 'tell application "System Events" to tell process "Terminal" to key code 52';
# }

# alias tt = "f(){
#   osascript 
#   -e 'tell application ""Terminal"" to activate' \
#   -e 'tell application ""System Events"" to tell process ""Terminal"" to keystroke ""t"" using command down' \
#   -e 'tell application ""System Events"" to tell process ""Terminal"" to keystroke "$instruction"' \
#   -e 'tell application ""System Events"" to tell process ""Terminal"" to key code 52' \
#   -e 'tell application ""System Events"" to tell process ""Terminal"" to keystroke ""exit""' \ 
#   -e 'tell application ""System Events"" to tell process ""Terminal"" to key code 52'; f}";
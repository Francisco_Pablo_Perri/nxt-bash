shopt -s expand_aliases
source ~/.bash_profile
source $NXT_BASH_PATH/.nxt_bash
##########################

FILE=$NXT_BASH_PATH/key-values.txt
touch -a $FILE

setKV() {
  KEY=$1
  VALUE=$2
  if [ ! -z $(grep "$KEY" "$FILE") ]; 
  then 
      sed -i "/$KEY=/d" $FILE 
  fi
  echo "$KEY=$VALUE"  >> $FILE
}

getKV() {
  KEY=$1
  cat $FILE | grep -w "$KEY" | cut -d'=' -f2
}

pullKV() {
  KEY=$1
  getKV $KEY 
  removeKv $KEY 
}

removeKv() {
  KEY=$1
  sed "/$KEY=/d" $FILE > $FILE
}

usage() { echo "usage:" && grep ".)\ #" $0; exit 0; }
[ $# -eq 0 ] && usage
while [ ! -z "$1" ]; do
  case "$1" in
set                 ) # Usage: "keyvalue set [key] [value]" | Description: Sets value for a given key.
  setKV $2 $3
  exit 0
;;
get                 ) # Usage: "keyvalue get [key]"         | Description: Returns value for a given key.
  getKV $2
  exit 0
;;
pull                ) # Usage: "keyvalue pull [key]"        | Description: Returns value for a given key and then removes entry. 
  pullKV $2
  exit 0
;;
remove              ) # Usage: "keyvalue remove [key]"      | Description: Removes entry for a given key. 
  removeKv $2
  exit 0
;;
ls                  ) # Usage: "keyvalue ls"                | Description: List all key/values 
  cat $FILE
  exit 0
;;
-h | --help         ) #                                     | Description: Print help
  usage
  exit 0
;;
*                   )
  echo "Unkown argument $1"
  usage;
  exit 0
;;
  esac
shift
done


